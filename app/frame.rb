class Frame
  attr_accessor :number, :game, :balls

  def initialize(number, game=nil)
    @game = game
    @balls = []
    @number = number
  end

  def add_ball(ball)
    if complete?
      raise ExcessBallsError.new("only #{max_balls_allowed} balls allowed in frame #{number}")
    end

    balls << ball
  end

  def strike?
    balls.first == 10
  end

  def spare?
    balls.count >= 2 && balls[0] + balls[1] == 10
  end

  def closed?
    strike? || spare?
  end

  def max_balls_allowed
    (number == 10 && closed?) ? 3 : strike? ? 1 : 2
  end

  def score
    score = pinfall
    score += game.score_from_next_balls(extra_balls_for_score, self.number) if closed?
    score
  end

  def complete?
    balls.count == max_balls_allowed
  end

  def pinfall(first_balls=max_balls_allowed)
    balls[0...first_balls].reduce(:+)
  end

  class ExcessBallsError < StandardError; end

private

  def extra_balls_for_score
    if strike?
      2
    elsif spare?
      1
    else
      0
    end
  end
end