require 'frame'

class Game
  attr_accessor :frames

  def initialize
    @frames = []
  end

  def add_ball(ball)
    if frames.empty?
      frames << Frame.new(1, self)
    end

    if frames.last.complete?
      frames << Frame.new(frames.count+1, self)
    end

    if frames.count > 10
      raise ExcessFramesError.new("only 10 frames allowed in game")
    end

    frames.last.add_ball ball
  end

  def score
    calculate_score(frames)
  end

  def score_after_frame(frame)
    calculate_score(frames[0..frame])
  end

  def score_from_next_balls(number, frame_number)
    next_frame = frames[frame_number]
    if next_frame
      if next_frame.strike?
        second_frame = frames[frame_number+1]
        if number == 2 && second_frame
          next_frame.pinfall + second_frame.pinfall(1)
        else
          next_frame.pinfall
        end
      else
        next_frame.pinfall(number)
      end
    else
      0
    end
  end

  class ExcessFramesError < StandardError; end

private

  def calculate_score(frames)
    frames.inject(0) { |sum, frame| sum += frame.score }
  end
end