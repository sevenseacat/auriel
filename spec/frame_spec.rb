require 'spec_helper'
require 'frame'

describe Frame do
  let(:f) { Frame.new(1) }

  describe 'validations' do
    context 'frames 1-9' do
      it 'only allows two balls per frame' do
        f.add_ball 1
        f.add_ball 2
        expect { f.add_ball 2 }.to raise_exception Frame::ExcessBallsError
      end
    end

    context 'tenth frame' do
      let(:f) { Frame.new(10) }

      context 'spare' do
        it 'allows three balls' do
          f.add_ball 3
          f.add_ball 7
          expect { f.add_ball 3 }.not_to raise_exception
        end
      end

      context 'strike' do
        it 'allows three balls' do
          f.add_ball 10
          f.add_ball 3
          expect { f.add_ball 3 }.not_to raise_exception
        end
      end

      context 'open frame' do
        it 'only allows two balls' do
          f.add_ball 2
          f.add_ball 3
          expect { f.add_ball 2 }.to raise_exception Frame::ExcessBallsError
        end
      end
    end
  end

  describe '#strike?' do
    it 'returns true for strikes' do
      f.add_ball 10
      f.should be_strike
    end

    it 'returns false for spares' do
      f.add_ball 3
      f.add_ball 7
      f.should_not be_strike
    end

    it 'returns false for open frames' do
      f.add_ball 3
      f.add_ball 5
      f.should_not be_strike
    end
  end

  describe '#spare?' do
    it 'returns false for strikes' do
      f.add_ball 10
      f.should_not be_spare
    end

    it 'returns true for spares' do
      f.add_ball 3
      f.add_ball 7
      f.should be_spare
    end

    it 'returns false for open frames' do
      f.add_ball 3
      f.add_ball 5
      f.should_not be_spare
    end
  end

  describe '#score' do
    it 'can calculate an incomplete frame' do
      f.add_ball 3
      f.score.should eq 3
    end

    it 'can calculate an open frame' do
      f.add_ball 4
      f.add_ball 2
      f.score.should eq 6
    end

    it 'can calculate a closed frame' do
      f.stub_chain(:game, :score_from_next_balls).with(1, anything) { 5 }
      f.add_ball 6
      f.add_ball 4
      f.score.should eq 15
    end
  end

  describe '#complete?' do
    it 'can account for open frames' do
      f.add_ball 2
      f.add_ball 3
      f.should be_complete
    end

    it 'can account for spares' do
      f.add_ball 3
      f.add_ball 7
      f.should be_complete
    end

    it 'can account for strikes' do
      f.add_ball 10
      f.should be_complete
    end

    it 'can account for incomplete frames' do
      f.add_ball 3
      f.should_not be_complete
    end
  end
end