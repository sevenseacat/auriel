require 'spec_helper'
require 'game'

describe Game do
  let(:g) { Game.new }

  describe 'validations' do
    it 'only allows ten frames' do
      12.times { g.add_ball 10 }
      expect { g.add_ball 10 }.to raise_exception Game::ExcessFramesError
    end
  end

  describe 'frames' do
    it 'can build new frames' do
      g.add_ball 2
      g.frames.count.should eq 1
    end

    describe 'multiple frames' do
      it 'creates new frames after strikes' do
        g.add_ball 10
        g.add_ball 2
        g.frames.count.should eq 2
      end

      it 'creates new frames after spares' do
        g.add_ball 5
        g.add_ball 5
        g.add_ball 4
        g.frames.count.should eq 2
      end

      it 'creates new frames after open balls' do
        g.add_ball 3
        g.add_ball 3
        g.add_ball 4
        g.add_ball 3
        g.add_ball 5
        g.frames.count.should eq 3
      end
    end
  end

  describe '#score' do
    it 'can score a spare' do
      g.add_ball 5
      g.add_ball 5
      g.add_ball 9
      g.add_ball 0
      g.score.should eq 28
    end

    it 'can score a strike' do
      g.add_ball 10
      g.add_ball 5
      g.add_ball 5
      g.score.should eq 30
    end

    it 'can score consecutive spares' do
      g.add_ball 6
      g.add_ball 4
      g.add_ball 6
      g.add_ball 4
      g.add_ball 6
      g.frames[0].score.should eq 16
      g.frames[1].score.should eq 16
      g.score.should eq 38
    end

    it 'can score consecutive strikes' do
      g.add_ball 10
      g.add_ball 10
      g.add_ball 8
      g.add_ball 2
      g.frames[0].score.should eq 28
      g.frames[1].score.should eq 20
      g.score.should eq 58
    end
  end

  # Scores taken from https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-snc7/577943_10152382351740858_183837903_n.jpg
  describe 'some complete games' do
    context "Cam's game" do
      let(:g) { Game.new }
      subject { g }

      before :each do
        [8,2, 10, 10, 10, 10, 10, 7,2, 7,2, 9,0, 9,1,8].each do |ball|
          g.add_ball ball
        end
      end

      [20, 50, 80, 110, 137, 156, 165, 174, 183, 201].each_with_index do |score, index|
        it "calculates the score for frame #{index} correctly" do
          g.score_after_frame(index).should eq score
        end
      end

      its(:score) { should eq 201 }
    end

    context "Thuc's game" do
      let(:g) { Game.new }
      subject { g }

      before :each do
        [9,0, 10, 9,1, 9,1, 7,3, 10, 10, 9,0, 10, 9,1,9].each do |ball|
          g.add_ball ball
        end
      end

      [9, 29, 48, 65, 85, 114, 133, 142, 162, 181].each_with_index do |score, index|
        it "calculates the score for frame #{index} correctly" do
          g.score_after_frame(index).should eq score
        end
      end

      its(:score) { should eq 181 }
    end

    context "my game" do
      let(:g) { Game.new }
      subject { g }

      before :each do
        [7,1, 9,0, 10, 10, 10, 10, 10, 10, 10, 6,1].each do |ball|
          g.add_ball ball
        end
      end

      [8, 17, 47, 77, 107, 137, 167, 193, 210, 217].each_with_index do |score, index|
        it "calculates the score for frame #{index} correctly" do
          g.score_after_frame(index).should eq score
        end
      end

      its(:score) { should eq 217 }
    end
  end
end